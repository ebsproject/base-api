FROM wso2/wso2mi:4.0.0-ubuntu
WORKDIR /home/wso2carbon

ENV spring.datasource.url=jdbc:postgresql://{hostname}:{port}/{databasename}
ENV spring.datasource.username=username
ENV spring.datasource.password=password

COPY dataflows/integratorCompositeApplication/target ./wso2mi-4.0.0/repository/deployment/server/carbonapps
COPY entrypoint.sh target/ebs-sg-ex.jar ./

ENTRYPOINT ["./entrypoint.sh"]
