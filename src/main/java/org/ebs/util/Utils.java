package org.ebs.util;

import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class Utils {

    private static final String GEOJSON_TEMPLATE = "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"{geometry}\",\"coordinates\":{coordinates}}}]}";

    /**
     * Ignores properties in which the source and target have different types for a
     * given attribute name or if the attribute's value in the source is null
     *
     * @param source
     * @param target
     * @return an array with properties which must not be copied from the source to
     *         the target
     */
    private static String[] getPropsToIgnore(Object source, Object target) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        final BeanWrapper wrappedTarget = new BeanWrapperImpl(target);

        return Stream.of(wrappedSource.getPropertyDescriptors()).map(FeatureDescriptor::getName).filter(n -> {
            String sourceType = wrappedSource.getPropertyTypeDescriptor(n).getResolvableType().toString();
            String targetType = Optional.ofNullable(wrappedTarget.getPropertyTypeDescriptor(n))
                    .map(typeDes -> typeDes.getResolvableType().toString()).orElse(null);

            return !sourceType.equals(targetType) || wrappedSource.getPropertyValue(n) == null;
        }).toArray(String[]::new);

    }

    /**
     * Copy properties between beans which are not null values in the source
     *
     * @param source object to be copied
     * @param target object receiving the copied values
     */
    public static void copyNotNulls(Object source, Object target) {
        BeanUtils.copyProperties(source, target, getPropsToIgnore(source, target));
    }

    /**
     * Returns a valid geojson object with the following structure: 1 Feature
     * Collection, with one Feature and one Geometry. The geometry type is set based
     * on the number of coordinates provided
     *
     * @param coordinaes in a postgreSQL notation. Example:
     *                   "((33.9447710680755,-9.55896224165781))"
     * @return the geojson representation of the coordinates
     */
    public static String rawGeojsonFromCoordinates(String coordinates) {

        Pattern pattern = Pattern.compile("\\([+-]?([0-9]*[.])?[0-9]+(?:,\\s*[+-]?([0-9]*[.])?[0-9]+)+\\)");
        List<String> list = new ArrayList<String>();

        // Matching the compiled pattern in the String
        Matcher matcher = pattern.matcher(coordinates);
        while (matcher.find()) {
            String formattedCoordinates = matcher.group().replaceAll("\\(", "[").replaceAll("\\)", "]");
            list.add(formattedCoordinates);
        }
        coordinates = null;
        String geometricType = null;

        if (list.size() > 3) {
            coordinates = String.join(",", list);
            coordinates = "[[" + coordinates + "]]";
            geometricType = "Polygon";
        } else if (list.size() == 1) {
            coordinates = String.join(",", list);
            coordinates = coordinates.replaceAll("\\(", "[").replaceAll("\\)", "]");
            geometricType = "Point";
        }

        return GEOJSON_TEMPLATE.replace("{coordinates}", coordinates).replace("{geometry}", geometricType);
    }

    /**
     * Adds leading zeroes to a numeric value if the number of digit is less than
     * size
     *
     * @param value numeric value
     * @param size  of the resulting String
     * @return String representation of the number, or empty if null or 0
     */
    public static final String getPaddingZeroes(Integer value, int size) {
        return (value == null || value.equals(0)) ? "" : String.format("%0" + size + "d", value);
    }

}